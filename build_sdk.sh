#!/bin/bash
IFS=$'\n'
for var in $(git log -1 --raw | grep ^: | awk '$5 != "D" { print $6}' | grep -Po '(SDK\/[a-z\-]+\/)' | uniq);
do
  echo "$var"; cd $var; npm install; npm run prepublish; npm publish;
done